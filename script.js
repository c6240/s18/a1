
const getCube = 2 ** 3
console.log(`The cube of 2 is ${getCube}`);

let address =[`258 Washington AVE NW`, `California`, 90011 ];

let [street, country, zipcode] = address;
console.log(`I live at ${street}, ${country} ${zipcode}`);

let animal = {
	name:`Lolong`,
	type:`saltwater crocodile`,
	weight:`1075`,
	measurement:`20 ft 3 in`
}

let {name,type,weight,measurement} = animal
console.log(`${name} was a ${type}. He weighed ${weight} kgs with a measurement of ${measurement}.`);

let numbers = [1,2,3,4,5];

numbers.forEach(numbers => console.log(numbers));

let reduceNumber = numbers.reduce(nums => {
	return 1+2+3+4+5;
}) 
console.log(reduceNumber);

class Dog{
		constructor(Name, age, breed){
			this.Name = Name;
			this.age = age;
			this.breed = breed;
		}
	}
let arf = new Dog(`Frankie`,5,`Miniature Dachshund`)

console.log(arf);